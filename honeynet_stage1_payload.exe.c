#include "stage1_shellcode.h"

void (*shellcode)();

void main() {
  shellcode = (void (*)()) data;

  __asm ("int3"); // debugger trap
  shellcode();
}


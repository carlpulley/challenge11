#!/usr/bin/env python

"""
Honeynet Challenge 11 (https://honeynet.org/node/829)

Files present in CWD:
  fc_0.pcap - MD5: 6d3342b94b6f5e623a8867a4a5dca08e
  honeynet_stage2_payload.bin - MD5: 6cd807ebdc936f5123821f6d4f43fe81

Creates:
  testing output written to stdout
  
We assume that all function tests only manipulate function stack arguments.

Dr. Carl Pulley (c.j.pulley@hud.ac.uk)
"""

from pydbg import *
from pydbg.defines import *
import pefile
import struct
import sys
import string
import zlib
import StringIO

from common import *

import big_int_zadd
import big_int_add
import big_int_sub
import big_int_lsl
import big_int_asr
import big_int_gcd
import big_int_mult
import big_int_exp
import adler32
  
dummy_process = "C:\WINDOWS\system32\winmine.exe"
network_socket = 33
expected_stage2_data_addr = 0x284AD000
expected_stage2_start_offset = 0xD4
stage2_password = 0x1090f056
MEM_RESERVE = 0x2000

fd = open("honeynet_stage2_payload.bin", "rb")
stage2 = fd.read()
fd.close()

def setup_testing(dbg):
  if dbg.dbg.u.Exception.dwFirstChance:
    dbg.virtual_alloc(expected_stage2_data_addr, len(stage2), MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE)
    dbg.write_process_memory(expected_stage2_data_addr, stage2)
    dbg.out.write("Stage 2 testing: successfully attached to and initialised PID %d (OEP: 0x%08X)\n" % (int(dbg.pid), expected_stage2_data_addr + expected_stage2_start_offset))
    dbg.write_process_memory(dbg.context.Esp + 4, struct.pack("L", network_socket))
    dbg.write_process_memory(dbg.context.Esp + 2*4, struct.pack("L", expected_stage2_data_addr))
    dbg.write_process_memory(dbg.context.Esp + 3*4, struct.pack("L", expected_stage2_start_offset))
    dbg.write_process_memory(dbg.context.Esp + 4*4, struct.pack("L", 0x00160014))
    dbg.write_process_memory(dbg.context.Esp + 5*4, struct.pack("L", stage2_password))
    dbg.write_process_memory(dbg.context.Esp + 6*4, struct.pack("L", expected_stage2_data_addr))
    dbg.out.write("Stage 2 testing: running function tests...\n\n")
    function_tests = big_int_zadd.tests + \
      big_int_add.tests + \
      big_int_sub.tests + \
      big_int_lsl.tests + \
      big_int_asr.tests + \
      big_int_gcd.tests + \
      big_int_mult.tests + \
      big_int_exp.tests + \
      adler32.tests
    
    process_tests(function_tests)
    
    return DBG_CONTINUE
  else:
    dbg.out.write("Stage 2 testing: terminating process unexpectably!\n")
    dbg.terminate_process()
    return DBG_EXCEPTION_NOT_HANDLED

dbg = pydbg()
dbg.out = sys.stdout
dbg.load(dummy_process)
pe = pefile.PE(dummy_process)
entrypoint = pe.OPTIONAL_HEADER.ImageBase + pe.OPTIONAL_HEADER.AddressOfEntryPoint
dbg.bp_set(entrypoint, handler = setup_testing)
dbg.run()
dbg.out.write("Stage 2 testing: ...completed testing\n")
dbg.out.close()
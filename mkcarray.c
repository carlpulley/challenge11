/*
 * $Id: mkcarray.c 1698 2005-07-26 16:55:33Z $
 *
 * mkcarray - create c-style array from binary file.
 *
 * gcc -Wall -Werror mkcarray.c -o mkcarray
 *
 */
 
#include <stdio.h>
#include <ctype.h>
 
#define uint unsigned int
#define byte unsigned char
 
int main(int argc, char **argv)
{
        FILE *f;
        byte buffer[16];
        uint bytesRead, totalBytes = 0, i;
 
        if( argc != 2 )
        {
                printf("usage: %s FILE\n", argv[0]);
        }
        else
        {
                if( (f = fopen(argv[1], "rb")) )
                {
                        printf("unsigned char data[] = {\n");
 
                        while( !feof(f) )
                        {
                                bytesRead = fread((void *)buffer, 1, sizeof(buffer), f);
 
                                if( bytesRead > 0 )
                                {
//                                      printf("\t");
 
                                        for( i = 0; i < 16; i++ )
                                        {
                                                if ( i == 8 )
                                                        printf("  ");
 
                                                if( i < bytesRead )
                                                        printf("0x%02x, ", buffer[i]);
                                                else
                                                        printf("      ");
                                        }
 
                                        printf("   // 0x%04x  ", totalBytes);
 
                                        for( i = 0; i < bytesRead; i++ )
                                        {
                                                if ( i == 8 )
                                                printf("  ");
 
                                                if( isprint(buffer[i]) )
                                                        printf("%c", buffer[i]);
                                                else
                                                        printf(".");
                                        }
 
                                        totalBytes += bytesRead;
 
                                        printf("\n");
                                }
                        }
 
                        printf("};\n");
                }
                else
                {
                }
        }
 
        return 0;
}

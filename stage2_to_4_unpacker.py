#!/usr/bin/env python

"""
Honeynet Challenge 11 (https://honeynet.org/node/829)

Files present in CWD:
  fc_0.pcap - MD5: 6d3342b94b6f5e623a8867a4a5dca08e
  honeynet_stage1_payload.bin - MD5: 4585dd58f3b78f5d753b2e145bb5a09c

Creates:
  honeynet_stage2_payload.bin - MD5: 6cd807ebdc936f5123821f6d4f43fe81
  honeynet_stage3_payload.bin - MD5: e51269b54dd38f27d0b93e70ecec5b3f
  honeynet_stage4_payload.section0.elf - MD5: 2c659b3003bd51ca3ba62a8f4db287e1
  honeynet_stage4_payload.section1.elf - MD5: bec7ab003e5988d2ba98140f90ec60df
  stage2_to_4_unpacker.log - logging output from running this script

Dr. Carl Pulley (c.j.pulley@hud.ac.uk)
"""

from scapy.all import *
from pydbg import *
from pydbg.defines import *
import pefile
import struct
import sys
import string
import getopt
import traceback

dummy_process = "C:\WINDOWS\system32\winmine.exe"
fc_0 = "fc_0.pcap"
start_packet = 14
expected_stage2_data_addr = 0x284AD000
free_fd = 42
logging_file = "stage2_to_4_unpacker.log"
MEM_RESERVE = 0x2000

fd = open("honeynet_stage1_payload.bin", "rb")
stage1 = fd.read()
fd.close()
pkts = rdpcap(fc_0)

def remove_retransmissions(pkts):
  if len(pkts) < 2:
    return pkts
  elif pkts[0].haslayer(TCP) and pkts[1].haslayer(TCP) and pkts[0]["TCP"].payload == pkts[1]["TCP"].payload:
    return remove_retransmissions(pkts[1:])
  else:
    return [pkts[0]] + remove_retransmissions(pkts[1:])

read_pkts = remove_retransmissions([ p for p in pkts[14-1 : -1] if p["IP"].src == "10.0.0.2" and len(p["TCP"].payload) > 0 ])
write_pkts = [ p for p in pkts[14-1 : -1] if p["IP"].src == "10.0.0.1" and len(p["TCP"].payload) > 0 ]

def next_packet(pkt_ptr, pkt_offset, read_size):
  if pkt_offset + read_size < len(read_pkts[pkt_ptr]["TCP"].payload):
    return pkt_ptr, pkt_offset + read_size
  else:
    return pkt_ptr + 1, 0

def FreeBSD_recvfrom(dbg):
  s = dbg.get_arg(1)
  buf = dbg.get_arg(2)
  length = dbg.get_arg(3)
  flags = dbg.get_arg(4)
  _from = dbg.get_arg(5)
  _fromlenaddr = dbg.get_arg(6)
  flags_str = []
  if flags & 0x1:
    flags_str.append("MSG_OOB")
  if (flags >> 1) & 0x1:
    flags_str.append("MSG_PEEK")
  if (flags >> 2) & 0x1:
    flags_str.append("MSG_DONTROUTE")
  if (flags >> 3) & 0x1:
    flags_str.append("MSG_EOR")
  if (flags >> 4) & 0x1:
    flags_str.append("MSG_TRUNC")
  if (flags >> 5) & 0x1:
    flags_str.append("MSG_CTRUNC")
  if (flags >> 6) & 0x1:
    flags_str.append("MSG_WAITALL")
  if (flags >> 7) & 0x1:
    flags_str.append("MSG_DONTWAIT")
  if (flags >> 8) & 0x1:
    flags_str.append("MSG_EOF")
  if (flags >> 13) & 0x1:
    flags_str.append("MSG_NOTIFICATION")
  if (flags >> 14) & 0x1:
    flags_str.append("MSG_NBIO")
  if (flags >> 15) & 0x1:
    flags_str.append("MSG_COMPAT")
  dbg.out.write(" - recvfrom(s: 0x%08X, buf: 0x%08X, len: 0x%08X, flags: 0x%08X [%s], from: 0x%08X, fromlenaddr: 0x%08X)\n" % (s, buf, length, flags, string.join(flags_str, "|"), _from, _fromlenaddr))
  dbg.out.write("\n")
  dbg.out.write("Reading packet at index %d\n" % (dbg.read_ptr))
  data = read_pkts[dbg.read_ptr]["TCP"].payload
  dbg.write_process_memory(buf, str(data))
  eax = len(data)
  dbg.set_register("EAX", eax)
  if "MSG_PEEK" not in flags_str:
    dbg.read_ptr, dbg.read_offset = next_packet(dbg.read_ptr, dbg.read_offset, length)
  return DBG_CONTINUE

def FreeBSD_mmap(dbg):
  if dbg.stage2_reserved:
    addr = dbg.get_arg(1)
  else:
    addr = expected_stage2_data_addr
  length = dbg.get_arg(2)
  prot = dbg.get_arg(3)
  flags = dbg.get_arg(4)
  fd = dbg.get_arg(5)
  pos = dbg.get_arg(6)
  prot_str = []
  if prot == 0x0:
    prot_str = ["PROT_NONE"]
  if prot & 0x1:
    prot_str.append("PROT_READ")
  if (prot >> 1) & 0x1:
    prot_str.append("PROT_WRITE")
  if (prot >> 2) & 0x1:
    prot_str.append("PROT_EXEC")
  flags_str = []
  if flags == 0x0:
    flags_str = ["MAP_FILE"]
  if flags & 0x1:
    flags_str.append("MAP_SHARED")
  if (flags >> 1) & 0x1:
    flags_str.append("MAP_PRIVATE")
  if (flags >> 4) & 0x1:
    flags_str.append("MAP_FIXED")
  if (flags >> 5) & 0x1:
    flags_str.append("MAP_RENAME")
  if (flags >> 6) & 0x1:
    flags_str.append("MAP_NORESERVE")
  if (flags >> 9) & 0x1:
    flags_str.append("MAP_HASSEMAPHORE")
  if (flags >> 10) & 0x1:
    flags_str.append("MAP_STACK")
  if (flags >> 11) & 0x1:
    flags_str.append("MAP_NOSYNC")
  if (flags >> 12) & 0x1:
    flags_str.append("MAP_ANON")
  dbg.out.write(" - mmap(addr: 0x%08X, len: 0x%08X, prot: 0x%08X [%s], flags: 0x%08X [%s], fd: 0x%08X, pos: 0x%08X)\n" % (addr, length, prot, string.join(prot_str, "|"), flags, string.join(flags_str, "|"), fd, pos))
  if addr == 0x0:
    eax = dbg.virtual_alloc(None, length, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE)
  else:
    eax = dbg.virtual_alloc(addr, length, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE)
  if not dbg.stage2_reserved and eax != addr:
    dbg.out.write("WARNING: could not reserve memory pages starting at address 0x%08X (reserved from 0x%08X instead!)\n" % (addr, eax))
    # Manual patch/bodge to ensure allocation page size differences are handled correctly!
    if addr == expected_stage2_data_addr and eax == addr & 0xFFFF0000:
      eax = addr
      dbg.out.write(" - bodge applied to fake memory pages allocation starting at requested address 0x%08X\n" % (eax))
  dbg.set_register('EAX', eax)
  if dbg.stage2_reserved and dbg.stage3_reserved and dbg.stage4_section0_reserved and not dbg.stage4_section1_reserved:
    dbg.stage4_section1_addr = eax
    dbg.stage4_section1_len = length
    if dbg.trace4Out:
      dbg.set_callback(EXCEPTION_SINGLE_STEP, trace_stage4_instructions)
    dbg.stage4_section1_reserved = True
  if dbg.stage2_reserved and dbg.stage3_reserved and not dbg.stage4_section0_reserved and not dbg.stage4_section1_reserved:
    dbg.stage4_section0_addr = eax
    dbg.stage4_section0_len = length
    dbg.stage4_section0_reserved = True
  if dbg.stage2_reserved and not dbg.stage3_reserved and not dbg.stage4_section0_reserved and not dbg.stage4_section1_reserved:
    dbg.stage3_data_addr = eax
    dbg.stage3_data_len = length
    if dbg.trace3Out:
      dbg.set_callback(EXCEPTION_SINGLE_STEP, trace_stage3_instructions)
    dbg.stage3_reserved = True
  if not dbg.stage2_reserved and not dbg.stage3_reserved and not dbg.stage4_section0_reserved and not dbg.stage4_section1_reserved:
    dbg.stage2_data_addr = eax
    dbg.stage2_data_len = length
    if dbg.trace2Out:
      dbg.set_callback(EXCEPTION_SINGLE_STEP, trace_stage2_instructions)
    dbg.stage2_reserved = True
  return DBG_CONTINUE

def FreeBSD_write(dbg):
  fd = dbg.get_arg(1)
  buf = dbg.get_arg(2)
  nbyte = dbg.get_arg(3)
  dbg.out.write(" - write(fd: 0x%08X, *buf: 0x%08X, nbyte: 0x%08X)\n" % (fd, buf, nbyte))
  if fd == 0:
    data_written = dbg.read_process_memory(buf, nbyte)
    data_sent = str(write_pkts[dbg.write_ptr]["TCP"].payload)
    dbg.out.write("\n")
    dbg.out.write("Writing packet at index %d\n" % (dbg.write_ptr))
    if data_written != data_sent:
      dbg.out.write("\n")
      dbg.out.write("PACKET WRITE MISMATCH\n")
      dbg.out.write("\n")
      dbg.out.write("ACTUALLY WROTE:\n")
      dbg.out.write(dbg.hex_dump(data_written)+"\n")
      dbg.out.write("EXPECTED TO WRITE:\n")
      dbg.out.write(dbg.hex_dump(data_sent))
      dbg.write_process_memory(buf, data_sent)
      dbg.out.write("[*] patched memory buffer (0x%08X) to fake the sending of the expected packet\n" % buf)
    eax = nbyte
    dbg.set_register("EAX", eax)
    dbg.write_ptr = dbg.write_ptr + 1
    return DBG_CONTINUE
  else:
    dbg.out.write("\n")
    dbg.out.write("ERROR: attempting to write to file descriptor #%d - don't know what to do!\n" % fd)
    dbg.out.write("\n")
    dbg.out.write(dbg.dump_context(dbg.get_thread_context(thread_handle = dbg.h_thread))+"\n")
    dbg.terminate_process()
    return DBG_EXCEPTION_NOT_HANDLED

def FreeBSD_read(dbg):
  fd = dbg.get_arg(1)
  buf = dbg.get_arg(2)
  nbyte = dbg.get_arg(3)
  dbg.out.write(" - read(fd: 0x%08X, *buf: 0x%08X, nbyte: 0x%08X)\n" % (fd, buf, nbyte))
  if fd == 0:
    dbg.out.write("\n")
    dbg.out.write("Reading packet at index %d, offset %d\n" % (dbg.read_ptr, dbg.read_offset))
    data = str(read_pkts[dbg.read_ptr]["TCP"].payload)[dbg.read_offset:(dbg.read_offset + nbyte)]
    dbg.write_process_memory(buf, data)
    eax = len(data)
    dbg.set_register('EAX', eax)
    dbg.read_ptr, dbg.read_offset = next_packet(dbg.read_ptr, dbg.read_offset, nbyte)
    return DBG_CONTINUE
  elif fd == 42 and nbyte == 0x30:
    dbg.out.write("\n")
    dbg.out.write("Reading %d bytes from /dev/urandom and writing to 0x%08X\n" % (nbyte, buf))
    random_data = "\x2b\x97\xab\x92\x08\xa6\x0a\x16\xb1\x64\x57\x85\x99\xb5\x10\x6f\x6b\x45\x7f\xbd\x33\x0b\xfa\xc9\x34\xca\x58\x38\xe9\x75\x21\xf6\xd9\x8f\x80\x86\xdb\x67\x1e\xa6\x76\x16\x8c\xee\x27\x4c\x8b\xe7"
    dbg.out.write("\n")
    dbg.out.write("READ:\n")
    dbg.out.write(dbg.hex_dump(random_data)+"\n")
    dbg.write_process_memory(buf, random_data)
    eax = len(random_data)
    dbg.set_register('EAX', eax)
    return DBG_CONTINUE
  else:
    dbg.out.write("\n")
    dbg.out.write("ERROR: attempting to read from file descriptor #%d (0x%X) - don't know what to do!\n" % (fd, fd))
    dbg.terminate_process()
    return DBG_EXCEPTION_NOT_HANDLED

def FreeBSD_open(dbg):
  path = dbg.get_arg(1)
  flags= dbg.get_arg(2)
  mode = dbg.get_arg(3)
  path_str = dbg.get_ascii_string(dbg.read_process_memory(path, 64))
  if len(path_str) == 64:
    path_str = path_str + ".."
  flags_str = []
  if flags == 0x0:
    flags_str.append("O_RDONLY")
  if (flags >> 1) & 0x1:
    flags_str.append("O_WRONLY")
  if (flags >> 2) & 0x1:
    flags_str.append("O_RDWR")
  dbg.out.write(" - open(path: 0x%08X [%s], flags: 0x%08X [%s], mode: 0x%08X)\n" % (path, path_str, flags, string.join(flags_str, "|"), mode))
  dbg.out.write("\n")
  dbg.out.write("Free file descriptor #%d (0x%X) allocated\n" % (dbg.free_fd, dbg.free_fd))
  eax = dbg.free_fd
  dbg.free_fd = dbg.free_fd + 1
  dbg.set_register('EAX', eax)
  return DBG_CONTINUE
  
def FreeBSD_getpid(dbg):
  dbg.out.write(" - getpid()\n")
  dbg.out.write("\n")
  eax = dbg.pid
  dbg.out.write("Returning PID: 0x%08X [%d]\n" % (eax, eax))
  dbg.set_register('EAX', eax)
  return DBG_CONTINUE

def FreeBSD_sysctl(dbg):
  namep = dbg.get_arg(1)
  namelen= dbg.get_arg(2)
  oldp = dbg.get_arg(3)
  oldlenp = dbg.get_arg(4)
  newp = dbg.get_arg(5)
  newlen = dbg.get_arg(6)
  def sysctl_table(level, key):
    level0 = ["CTL_UNSPEC", "CTL_KERN", "CTL_VM", "CTL_VFS", "CTL_NET", "CTL_DEBUG", "CTL_HW", "CTL_MACHDEP", "CTL_USER", "CTL_P1003_1B"]
    kern_level1 = [None, "KERN_OSTYPE", "KERN_OSRELEASE", "KERN_OSREV", "KERN_VERSION", "KERN_MAXVNODES", "KERN_MAXPROC", "KERN_MAXFILES", "KERN_ARGMAX", "KERN_SECURELVL", "KERN_HOSTNAME", "KERN_HOSTID", "KERN_CLOCKRATE", "KERN_VNODE", "KERN_PROC", "KERN_FILE", "KERN_PROF", "KERN_POSIX1", "KERN_NGROUPS", "KERN_JOB_CONTROL", "KERN_SAVED_IDS", "KERN_BOOTTIME", "KERN_NISDOMAINNAME", "KERN_UPDATEINTERVAL", "KERN_OSRELDATE", "KERN_NTP_PLL", "KERN_BOOTFILE", "KERN_MAXFILESPERPROC", "KERN_MAXPROCPERUID", "KERN_DUMPDEV", "KERN_IPC", "KERN_DUMMY", "KERN_PS_STRINGS", "KERN_USRSTACK", "KERN_LOGSIGEXIT", "KERN_IOV_MAX", "KERN_HOSTUUID", "KERN_ARND"]
    kern_level2 = ["KERN_PROC_ALL", "KERN_PROC_PID", "KERN_PROC_PGRP", "KERN_PROC_SESSION", "KERN_PROC_TTY", "KERN_PROC_UID", "KERN_PROC_RUID", "KERN_PROC_ARGS", "KERN_PROC_PROC", "KERN_PROC_SV_NAME", "KERN_PROC_RGID", "KERN_PROC_GID", "KERN_PROC_PATHNAME", "KERN_PROC_OVMMAP", "KERN_PROC_OFILEDESC", "KERN_PROC_KSTACK", "KERN_PROC_INC_THREAD", "KERN_PROC_INC_THREAD", "KERN_PROC_INC_THREAD", "KERN_PROC_INC_THREAD", "KERN_PROC_INC_THREAD", "KERN_PROC_INC_THREAD", "KERN_PROC_INC_THREAD", "KERN_PROC_INC_THREAD", "KERN_PROC_INC_THREAD", "KERN_PROC_INC_THREAD", "KERN_PROC_INC_THREAD", "KERN_PROC_INC_THREAD", "KERN_PROC_INC_THREAD", "KERN_PROC_INC_THREAD", "KERN_PROC_INC_THREAD", "KERN_PROC_INC_THREAD", "KERN_PROC_VMMAP", "KERN_PROC_FILEDESC", "KERN_PROC_GROUPS", "KERN_PROC_ENV", "KERN_PROC_AUXV", "KERN_PROC_RLIMIT", "KERN_PROC_PS_STRINGS", "KERN_PROC_UMASK", "KERN_PROC_OSREL"]
    if level == 0:
      return (level0[key], key)
    elif level == 1:
      return (kern_level1[key], key)
    elif level == 2:
      return (kern_level2[key], key)
    elif level == 3:
      return ("PID", key)
    else:
      return ("", "ERROR - level: %d; key: %s" % (level, key))
  names = [ sysctl_table(i, struct.unpack("l", dbg.read_process_memory(namep + i*4, 4))[0]) for i in range(0, namelen) ]
  raw_names = [ nm for nm, k in names ]
  names_str = "{ " + string.join([ string.join([nm, "[%s]"%k], " ") for nm, k in names ], ", ") + " }"
  dbg.out.write(" - sysctl(*name: 0x%08X, namelen: 0x%08X %s; *old: 0x%08X, *oldlenp: 0x%08X; *new: 0x%08X, newlen: 0x%08X)\n" % (namep, namelen, names_str, oldp, oldlenp, newp, newlen))
  dbg.out.write("\n")
  if "CTL_KERN" in raw_names and "KERN_PROC" in raw_names and "KERN_PROC_VMMAP" in raw_names:
    # FIXME: should we be simulating this system function better?
    target_size = 0x10 + 0x8c
    target_addr = dbg.virtual_alloc(None, target_size, MEM_COMMIT | MEM_RESERVE, PAGE_EXECUTE_READWRITE)
    dbg.write_process_memory(dbg.get_register('EBP') - 0x403C, struct.pack("l", target_addr + target_size))
    eax = 0x0
  else:
    eax = 0xff
  dbg.set_register('EAX', eax)
  return DBG_CONTINUE

def FreeBSD_munmap(dbg):
  addr = dbg.get_arg(1)
  nbyte = dbg.get_arg(2)
  dbg.out.write(" - munmap(*addr: 0x%08X, len: 0x%08X)\n" % (addr, nbyte))
  dbg.out.write("\n")
  # We always perform a MEM_RELEASE here
  try:
    dbg.virtual_free(addr, nbyte, 0x8000)
  except:
    dbg.out.write("[*] WARNING: VirtualFree threw the exception (ignoring and pretending freeing of memory worked!):\n  - %s" % traceback.format_exc())
  eax = 0x0
  dbg.set_register('EAX', eax)
  return DBG_CONTINUE

def FreeBSD_mprotect(dbg):
  addr = dbg.get_arg(1)
  length = dbg.get_arg(2)
  prot = dbg.get_arg(3)
  dbg.out.write(" - mprotect(*addr: 0x%08X, len: 0x%08X, prot: 0x%08X)\n" % (addr, length, prot))
  dbg.out.write("\n")
  # Always provide the page with full protections
  dbg.virtual_protect(addr, length, PAGE_EXECUTE_READWRITE)
  eax = 0x0
  dbg.set_register('EAX', eax)
  return DBG_CONTINUE

def FreeBSD_close(dbg):
  fd = dbg.get_arg(1)
  dbg.out.write(" - close(fd: 0x%08X [%d])\n" % (fd, fd))
  dbg.out.write("\n")
  eax = 0x0
  dbg.set_register('EAX', eax)
  return DBG_CONTINUE

def FreeBSDSystemHook(dbg):
  if dbg.dbg.u.Exception.dwFirstChance:
    eax = dbg.get_register('EAX')
    eip = dbg.get_register('EIP')
    dbg.out.write("0x%08X (+0x%X): FreeBSD System function #%d (0x%X) called\n" % (eip, eip - dbg.payload_start_addr, int(eax), int(eax)))
    if eax == 0x1d:
      cont = FreeBSD_recvfrom(dbg)
    elif eax == 0x1dd:
      cont = FreeBSD_mmap(dbg)
    elif eax == 0x4:
      cont = FreeBSD_write(dbg)
    elif eax == 0x3:
      cont = FreeBSD_read(dbg)
    elif eax == 0x5:
      cont = FreeBSD_open(dbg)
    elif eax == 0x6:
      cont = FreeBSD_close(dbg)
    elif eax == 0x14:
      cont = FreeBSD_getpid(dbg)
    elif eax == 0xca:
      cont = FreeBSD_sysctl(dbg)
    elif eax == 0x49:
      cont = FreeBSD_munmap(dbg)
    elif eax == 0x4a:
      cont = FreeBSD_mprotect(dbg)
    else:
      dbg.out.write(" - ERROR: unexpected FreeBSD system function!\n")
      dbg.out.write("\n")
      dbg.out.write(dbg.dump_context(dbg.get_thread_context(thread_handle = dbg.h_thread))+"\n")
      dbg.terminate_process()
      return  DBG_EXCEPTION_NOT_HANDLED
    dbg.out.write("\n")
    dbg.out.write(dbg.dump_context(dbg.get_thread_context(thread_handle = dbg.h_thread))+"\n")
    return cont
  else:
    dbg.terminate_process()
    return DBG_EXCEPTION_NOT_HANDLED

def hook_FreeBSDSystemCalls(dbg, addr, size):
  # locate all stage 1 INT 80 instructions and hook them
  data = dbg.read_process_memory(addr, size)
  for offset in xrange(size):
    if "\xCD\x80" in data[offset : offset+2]:
      dbg.out.write("FreeBSD system call (i.e. INT 80) patched at 0x%08X [+0x%X]\n" % (addr + offset, offset))
      dbg.write_process_memory(addr+offset, "\x90\x90")
      dbg.bp_set(addr+offset, handler = FreeBSDSystemHook)
  dbg.payload_start_addr = addr

def hook_stage1_FreeBSDSystemCalls(dbg):
  hook_FreeBSDSystemCalls(dbg, dbg.stage1_start_addr, len(stage1))
  dbg.out.write("Stage 1: patched all FreeBSD system functions\n")
  dbg.out.write("\n")

def hook_stage2_FreeBSDSystemCalls(dbg):
  hook_FreeBSDSystemCalls(dbg, dbg.stage2_data_addr, dbg.stage2_data_len)
  dbg.out.write("Stage 2: patched all FreeBSD system functions\n")
  dbg.out.write("\n")

def hook_stage3_FreeBSDSystemCalls(dbg):
  hook_FreeBSDSystemCalls(dbg, dbg.stage3_data_addr, dbg.stage3_data_len)
  dbg.out.write("Stage 3: patched all FreeBSD system functions\n")
  dbg.out.write("\n")

def dump_FunctionContext(dbg, msg, function_callback=None):
  if dbg.dbg.u.Exception.dwFirstChance:
    dbg.out.write("Stage 2: %s at 0x%08X [+0x%X]\n" % (msg, dbg.context.Eip, dbg.context.Eip - dbg.stage2_data_addr))
    dbg.out.write("\n")
    dbg.out.write(dbg.dump_context(dbg.get_thread_context(thread_handle = dbg.h_thread)))
    if function_callback:
      function_callback(dbg)
    dbg.out.write("\n")
    return DBG_CONTINUE
  else:
    dbg.terminate_process()
    return DBG_EXCEPTION_NOT_HANDLED

def hook_stage2_PayloadFunctions(dbg):
  # Hook key Rabbit cipher functions
  def RABBIT_setup_callback(d):
    arg0_str = d.read_process_memory(struct.unpack("l", d.read_process_memory(d.context.Esp, 0x4))[0], 2*((8 + 8 + 1)*4))
    d.out.write("\nARG0 - ENCRYPT_ctx:")
    d.out.write(dbg.hex_dump(arg0_str))
    arg1_str = d.read_process_memory(struct.unpack("l", d.read_process_memory(d.context.Esp+0x4, 0x4))[0], 0x10)
    d.out.write("\nARG1 - 0x10 byte key:")
    d.out.write(dbg.hex_dump(arg1_str))
    arg2_str = d.read_process_memory(struct.unpack("l", d.read_process_memory(d.context.Esp+0x8, 0x4))[0], 0x8)
    d.out.write("\nARG2 - 0x8 byte IV:")
    d.out.write(dbg.hex_dump(arg2_str))
  dbg.bp_set(dbg.stage2_data_addr + 0x940, handler = lambda d: dump_FunctionContext(d, "entered RABBIT_setup", RABBIT_setup_callback))
  def RABBIT_process_bytes_callback(d):
    mem_str = d.read_process_memory(struct.unpack("l", d.read_process_memory(d.context.Esp+0x8, 0x4))[0], struct.unpack("l", d.read_process_memory(d.context.Esp+0xc, 0x4))[0])
    d.out.write("\n%d BYTE MEMORY HEXDUMP:" % len(mem_str))
    d.out.write(dbg.hex_dump(mem_str))
    if mem_str == "\x00\x10\x07\x08\x00\xf0\x08\x00\x48\x1f\x00\x00\x00\x00\x03":
      d.out.write("[*] applying to bodge to ensure stage 4, section 1 memory is located in Windows user space (by mapping 0x08071000 to 0x02071000)\n")
      d.write_process_memory(struct.unpack("l", d.read_process_memory(d.context.Esp+0x8, 0x4))[0], "\x00\x10\x07\x02")
  dbg.bp_set(dbg.stage2_data_addr + 0xa9d, handler = lambda d: dump_FunctionContext(d, "entered RABBIT_process_bytes", RABBIT_process_bytes_callback))
  dbg.bp_set(dbg.stage2_data_addr + 0xb40, handler = lambda d: dump_FunctionContext(d, "exited RABBIT_process_bytes", RABBIT_process_bytes_callback))
  dbg.out.write("Stage 2: hooked all stage 2 payload functions\n")
  dbg.out.write("\n") 

def stage3_error_exit(dbg):
  if dbg.dbg.u.Exception.dwFirstChance:
    dbg.out.write("Stage 3: hit ERROR exit point at 0x%08X [+0x%X]\n" % (dbg.context.Eip, dbg.context.Eip - dbg.stage3_data_addr))
    dbg.out.write("\n")
    dbg.out.write(dbg.dump_context(dbg.get_thread_context(thread_handle = dbg.h_thread))+"\n")
    return DBG_CONTINUE
  else:
    dbg.terminate_process()
    return DBG_EXCEPTION_NOT_HANDLED

def save_stage4(dbg):
  # We don't worry about first chance exceptions here as we'll be terminating!
  dbg.out.write("Stage 3: hit exit point at 0x%08X [+0x%X]\n" % (dbg.context.Eip, dbg.context.Eip - dbg.stage3_data_addr))
  dbg.out.write("\n")
  dbg.out.write(dbg.dump_context(dbg.get_thread_context(thread_handle = dbg.h_thread))+"\n")
  # Magic numbers here are offsets (pulled from data in log file)
  stage4_unpacked_section0 = dbg.read_process_memory(dbg.stage4_section0_addr + 0x8000, dbg.stage4_section0_len)
  stage4_unpacked_section1 = dbg.read_process_memory(dbg.stage4_section1_addr + 0x1000, dbg.stage4_section1_len)
  # Dump section 0
  fd = open("honeynet_stage4_payload.section0.elf", "wb")
  fd.write(stage4_unpacked_section0)
  fd.close()
  # Dump section 1
  fd = open("honeynet_stage4_payload.section1.elf", "wb")
  fd.write(stage4_unpacked_section1)
  fd.close()
  # Reconstruct ELF file (magic numbers here determined by examining ELF header data)
  fd = open("honeynet_stage4_payload.elf", "wb")
  fd.write(stage4_unpacked_section0[0:166273])
  fd.write("\x00" * (0x29000 - 166273))
  fd.write(stage4_unpacked_section1[0:8008])
  fd.close()
  dbg.out.write("Stage 3: stage 4 payload saved to honeynet_stage4_payload.elf\n")
  dbg.out.write("\nAll stages successfully unpacked.. exiting!\n")
  dbg.terminate_process()
  return DBG_EXCEPTION_NOT_HANDLED

def save_stage3(dbg):
  if dbg.dbg.u.Exception.dwFirstChance:
    dbg.out.write("Stage 2: hit exit point at 0x%08X [+0x%X]\n" % (dbg.context.Eip, dbg.context.Eip - dbg.stage2_data_addr))
    dbg.out.write("\n")
    dbg.out.write(dbg.dump_context(dbg.get_thread_context(thread_handle = dbg.h_thread))+"\n")
    stage3_unpacked_data = dbg.read_process_memory(dbg.stage3_data_addr, dbg.stage3_data_len)
    fd = open("honeynet_stage3_payload.bin", "wb")
    fd.write(stage3_unpacked_data)
    fd.close()
    dbg.out.write("Stage 2: stage 3 payload saved to honeynet_stage3_payload.bin\n\n")
    call_stage3_payload(dbg)
    return DBG_CONTINUE
  else:
    dbg.terminate_process()
    return DBG_EXCEPTION_NOT_HANDLED

def save_stage2(dbg):
  stage2_unpacked_data = dbg.read_process_memory(dbg.stage2_data_addr, dbg.stage2_data_len)
  fd = open("honeynet_stage2_payload.bin", "wb")
  fd.write(stage2_unpacked_data)
  fd.close()

def call_stage3_payload(dbg):
  hook_stage3_FreeBSDSystemCalls(dbg)
  dbg.out.write("Stage 3: calling stage 3 payload at 0x%08X [+0x%X]\n" % (dbg.context.Eax, dbg.context.Eax - dbg.stage3_data_addr))
  dbg.out.write(" - arg1: 0x%08X (stage2 base addr); arg2: 0x%08X (stage3 base addr); arg3: 0x%08X (&ENCRYPT_process_bytes); arg4: 0x%08X (RABBIT context); arg5: 0x%08X (stage3 size); arg6: 0x%08X (stage2 base addr + 2)\n" % (dbg.get_arg(0), dbg.get_arg(1), dbg.get_arg(2), dbg.get_arg(3), dbg.get_arg(4), dbg.get_arg(5)))
  dbg.bp_set(dbg.stage3_data_addr + 0x25e, handler = save_stage4)
  dbg.out.write("Stage 3: set a breakpoint on the stage 3 exit point\n\n")
  dbg.bp_set(dbg.stage3_data_addr + 0x337, handler = stage3_error_exit)
  dbg.out.write("Stage 3: set a breakpoint on the stage 3 ERROR exit point\n\n")
  dbg.out.write("Stage 3: set a probe breakpoint\n\n")

def call_stage2_payload(dbg):
  hook_stage2_FreeBSDSystemCalls(dbg)
  hook_stage2_PayloadFunctions(dbg)
  dbg.out.write("Stage 2: calling stage 2 payload at 0x%08X [+0x%X]\n" % (dbg.context.Eax, dbg.context.Eax - dbg.stage2_data_addr))
  dbg.out.write(" - arg1: 0x%08X (socket fd); arg2: 0x%08X (stage2 base addr)\n" % (dbg.get_arg(0), dbg.get_arg(1)))
  dbg.bp_set(dbg.stage2_data_addr + 0x72a, handler = save_stage3)
  dbg.out.write("Stage 2: set a breakpoint on the stage 2 exit point\n")

def third_decrypt_loop(dbg):
  dbg.out.write("Stage 1: stage 2 payload successfully unpacked\n")
  save_stage2(dbg)
  dbg.out.write("\n")
  dbg.out.write(dbg.dump_context(dbg.get_thread_context(thread_handle = dbg.h_thread))+"\n")
  dbg.out.write("Stage 1: stage 2 payload saved to honeynet_stage2_payload.bin (OEP is 0x%08X [+0x%X])\n" % (dbg.context.Eax, dbg.context.Eax - dbg.stage2_data_addr))
  dbg.out.write("\n")
  call_stage2_payload(dbg)
  return DBG_CONTINUE

def second_decrypt_loop(dbg):
  if dbg.dbg.u.Exception.dwFirstChance:
    if dbg.count < 0x32:
      dbg.count = dbg.count + 1
    else:
      dbg.out.write("Stage 1: second XOR decrypt loop executed\n")
      dbg.out.write(dbg.hex_dump(dbg.read_process_memory(dbg.stage1_start_addr, len(stage1)))+"\n")
      # patch remaining shellcode so that FreeBSD system functions are correctly emulated
      hook_stage1_FreeBSDSystemCalls(dbg)
      dbg.bp_set(dbg.stage1_start_addr + 0x11D, handler = third_decrypt_loop)
    return DBG_CONTINUE
  else:
    dbg.terminate_process()
    return DBG_EXCEPTION_NOT_HANDLED

def first_decrypt_loop(dbg):
  if dbg.dbg.u.Exception.dwFirstChance:
    if dbg.count < 0x3C:
      dbg.count = dbg.count + 1
    else:
      dbg.out.write("Stage 1: first XOR decrypt loop executed\n")
      dbg.out.write(dbg.hex_dump(dbg.read_process_memory(dbg.stage1_start_addr, len(stage1)))+"\n")
      dbg.count = -1
      dbg.bp_set(dbg.stage1_start_addr + 0x50, handler = second_decrypt_loop)
    return DBG_CONTINUE
  else:
    dbg.terminate_process()
    return DBG_EXCEPTION_NOT_HANDLED

def trace_stage1_instructions(dbg):
  if int(dbg.context.Eip) >= dbg.stage1_start_addr and int(dbg.context.Eip) <= dbg.stage1_start_addr + len(stage1):
    instr = dbg.disasm(dbg.context.Eip)
    if dbg.trace1Out:
      dbg.trace1Out.write("%08X: %s\n" % (dbg.context.Eip, instr))
    dbg.single_step(True)
  else:
    dbg.single_step(False)
  return DBG_CONTINUE

def trace_stage2_instructions(dbg):
  if int(dbg.context.Eip) >= dbg.stage2_data_addr and int(dbg.context.Eip) <= dbg.stage2_data_addr + dbg.stage2_data_len:
    instr = dbg.disasm(dbg.context.Eip)
    if dbg.trace2Out:
      dbg.trace2Out.write("%08X: %s\n" % (dbg.context.Eip, instr))
    dbg.single_step(True)
  else:
    dbg.single_step(False)
  return DBG_CONTINUE

def setup_stage1(dbg):
  if dbg.dbg.u.Exception.dwFirstChance:
    dbg.stage1_start_addr = dbg.virtual_alloc(0x0, len(stage1), MEM_COMMIT, PAGE_EXECUTE_READWRITE)
    dbg.write_process_memory(dbg.stage1_start_addr, stage1)
    dbg.set_register("EIP", dbg.stage1_start_addr)
    dbg.bp_set(dbg.stage1_start_addr + 0x28, handler=first_decrypt_loop)
    dbg.count = -1
    dbg.out.write("Stage 1: successfully attached to and initialised PID %d (OEP: 0x%08X)\n" % (int(dbg.pid), dbg.stage1_start_addr))
    dbg.out.write(dbg.hex_dump(dbg.read_process_memory(dbg.stage1_start_addr, len(stage1)))+"\n")
    if dbg.trace1Out:
      dbg.set_callback(EXCEPTION_SINGLE_STEP, trace_stage1_instructions)
    return DBG_CONTINUE
  else:
    dbg.terminate_process()
    return DBG_EXCEPTION_NOT_HANDLED

def usage():
    print """stage2_to_4_unpacker.py [--trace1 <trace output>] [--trace2 <trace output>] [--trace3 <trace output>] [--trace4 <trace output>] [-h]
        Unpack stage 2, 3 and 4 payloads.

        Options:
        --trace1 <trace output> - output file for stage 1 instruction trace (for feeding to VERA);
        --trace2 <trace output> - output file for stage 2 instruction trace (for feeding to VERA);
        --trace3 <trace output> - output file for stage 3 instruction trace (for feeding to VERA);
        --trace4 <trace output> - output file for stage 4 instruction trace (for feeding to VERA);
        -h or --help - print this message.
    """

try:
    opts, args = getopt.getopt(sys.argv[1:], "h", ["help", "trace1=", "trace2=", "trace3=", "trace4="])
except getopt.GetoptError, e:
    print(str(e))
    usage()
    sys.exit(1)
trace1OutPath = ""
trace2OutPath = ""
trace3OutPath = ""
trace4OutPath = ""
for o, a in opts:
    if o in ("-h", "--help"):
        #Print help
        usage()
        sys.exit()
    elif o in ("--trace1"):
        #Set path for the trace output
        trace1OutPath = a
    elif o in ("--trace2"):
        #Set path for the trace output
        trace2OutPath = a
    elif o in ("--trace3"):
        #Set path for the trace output
        trace3OutPath = a
    elif o in ("--trace4"):
        #Set path for the trace output
        trace3OutPath = a
dbg = pydbg()
dbg.trace1Out = None
dbg.trace2Out = None
dbg.trace3Out = None
dbg.trace4Out = None
if trace1OutPath != "":
  dbg.trace1Out = open(trace1OutPath, "w")
if trace2OutPath != "":
  dbg.trace2Out = open(trace2OutPath, "w")
if trace3OutPath != "":
  dbg.trace3Out = open(trace3OutPath, "w")
if trace4OutPath != "":
  dbg.trace4Out = open(trace4OutPath, "w")
dbg.read_ptr = 0
dbg.read_offset = 0
dbg.write_ptr = 0
dbg.stage2_reserved = False
dbg.stage3_reserved = False
dbg.stage4_section0_reserved = False
dbg.stage4_section1_reserved = False
dbg.free_fd = free_fd
dbg.out = open(logging_file, "wb")
dbg.load(dummy_process)
pe = pefile.PE(dummy_process)
entrypoint = pe.OPTIONAL_HEADER.ImageBase + pe.OPTIONAL_HEADER.AddressOfEntryPoint
dbg.bp_set(entrypoint, handler = setup_stage1)
dbg.run()
dbg.out.close()
if dbg.trace1Out:
  dbg.trace1Out.close()
if dbg.trace2Out:
  dbg.trace2Out.close()
if dbg.trace3Out:
  dbg.trace3Out.close()
if dbg.trace4Out:
  dbg.trace4Out.close()

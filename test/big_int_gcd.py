def extended_binary_gcd(x, y, div2 = lambda p: p // 2):
  g = 1
  while (x % 2 == 0) and (y % 2 == 0):
    x, y, g = div2(x), div2(y), 2*g
  u, v, A, B, C, D = x, y, 1, 0, 0, 1
  while u != 0:
    while (u % 2 == 0):
      u = div2(u)
      if (A - B) % 2 == 0:
        A, B = div2(A), div2(B)
      else:
        A, B = div2(A+y), div2(B-x)
    while (v % 2 == 0):
      v = div2(v)
      if (C - D) % 2 == 0:
        C, D = div2(C), div2(D)
      else:
        C, D = div2(C+y), div2(D-x)
    if u >= v:
      u, A, B = u - v, A - C, B - D
    else:
      v, C, D = v - u, C - A, D - B
  a, b = C, D  
  return a, b, g*v
  
def simulate_0xd5d(x, y, div2 = lambda p: p // 2):
  a, b, d = extended_binary_gcd(x, y, div2)
  return b % x

def test_part_function_0xd5d(dbg, args, expected, continuation):
  # big_int_gcd testing
  arg0, arg1, arg2, num_longs = args
  a_expected, b_expected = expected
  ESP = dbg.context.Esp - 4*(num_longs*4+4) - 5*4
  dbg.set_register("ESP", ESP)
  arg0_addr = ESP+num_longs*4*1+4+5*4
  arg1_addr = ESP+num_longs*4*2+4+5*4
  arg2_addr = ESP+num_longs*4*3+4+5*4
  dbg.write_process_memory(arg0_addr, arg0)
  dbg.write_process_memory(arg1_addr, arg1)
  dbg.write_process_memory(arg2_addr, arg2)
  dbg.write_process_memory(ESP+4, struct.pack("L", arg0_addr))
  dbg.write_process_memory(ESP+8, struct.pack("L", arg1_addr))
  dbg.write_process_memory(ESP+0xC, struct.pack("L", arg2_addr))
  dbg.write_process_memory(ESP+0x10, struct.pack("L", num_longs))
  def check_test(d):
    b_actual = d.read_process_memory(struct.unpack("L", d.read_process_memory(d.context.Ebp+0x8, 4))[0], num_longs*4)
    a_actual = d.read_process_memory(struct.unpack("L", d.read_process_memory(d.context.Ebp-0x38, 4))[0], num_longs*4)
    if a_actual == a_expected and b_actual== b_expected:
      d.out.write("PASSED\n")
    else:
      d.out.write("*** FAILED! ***\n\n")
      d.out.write("Stage 2 testing: function 0xD5D (big_int_gcd) called with arguments:\n")
      d.out.write("\narg0 ([ESP+4] = 0x%08X):\n"%arg0_addr)
      d.out.write(d.hex_dump(arg0))
      d.out.write("\narg1 ([ESP+8] = 0x%08X):\n"%arg1_addr)
      d.out.write(d.hex_dump(arg1))
      d.out.write("\narg2 ([ESP+0xC] = 0x%08X):\n"%arg2_addr)
      d.out.write(d.hex_dump(arg2))
      d.out.write("\nnumber of longs ([ESP+0x10]): %d\n"%num_longs)
      d.out.write("\nEXPECTED:\n")
      d.out.write("a:"+d.hex_dump(a_expected))
      d.out.write("b:"+d.hex_dump(b_expected))
      d.out.write("\nFOUND:\n")
      d.out.write("a:"+d.hex_dump(a_actual))
      d.out.write("b:"+d.hex_dump(b_actual))
      d.out.write("\n"+d.dump_context(d.get_thread_context(thread_handle = d.h_thread))+"\n")
    d.out.write("\n")
  function_test(dbg, 0xd5d, 0xf43, check_test, continuation)

def test_full_function_0xd5d(dbg, args, expected, continuation):
  # big_int_gcd testing
  arg0, arg1, arg2, num_longs = args
  ESP = dbg.context.Esp - 4*(num_longs*4+4) - 5*4
  dbg.set_register("ESP", ESP)
  arg0_addr = ESP+num_longs*4*1+4+5*4
  arg1_addr = ESP+num_longs*4*2+4+5*4
  arg2_addr = ESP+num_longs*4*3+4+5*4
  dbg.write_process_memory(arg0_addr, arg0)
  dbg.write_process_memory(arg1_addr, arg1)
  dbg.write_process_memory(arg2_addr, arg2)
  dbg.write_process_memory(ESP+4, struct.pack("L", arg0_addr))
  dbg.write_process_memory(ESP+8, struct.pack("L", arg1_addr))
  dbg.write_process_memory(ESP+0xC, struct.pack("L", arg2_addr))
  dbg.write_process_memory(ESP+0x10, struct.pack("L", num_longs))
  def check_test(d):
    actual = d.read_process_memory(struct.unpack("L", d.read_process_memory(d.context.Ebp+0x8, 4))[0], num_longs*4)
    if actual == expected:
      d.out.write("PASSED\n")
    else:
      d.out.write("*** FAILED! ***\n\n")
      d.out.write("Stage 2 testing: function 0xD5D (big_int_gcd) called with arguments:\n")
      d.out.write("\narg0 ([ESP+4] = 0x%08X):\n"%arg0_addr)
      d.out.write(d.hex_dump(arg0))
      d.out.write("\narg1 ([ESP+8] = 0x%08X):\n"%arg1_addr)
      d.out.write(d.hex_dump(arg1))
      d.out.write("\narg2 ([ESP+0xC] = 0x%08X):\n"%arg2_addr)
      d.out.write(d.hex_dump(arg2))
      d.out.write("\nnumber of longs ([ESP+0x10]): %d\n"%num_longs)
      d.out.write("\nEXPECTED:\n")
      d.out.write(d.hex_dump(expected))
      d.out.write("\nFOUND:\n")
      d.out.write(d.hex_dump(actual))
      d.out.write("\n"+d.dump_context(d.get_thread_context(thread_handle = d.h_thread))+"\n")
    d.out.write("\n")
  function_test(dbg, 0xd5d, 0xf68, check_test, continuation)

tests = [  # big_int_gcd tests
  lambda c: test_part_function_0xd5d(dbg, ["\x01\x00\x00\x00", "\x02\x00\x00\x00", "\x08\x00\x00\x00", 1], ["\x01\x00\x00\x00", "\x00\x00\x00\x00"], c),
  lambda c: test_part_function_0xd5d(dbg, ["\x42\x42\x42\x42", "\x02\x00\x00\x00", "\x08\x00\x00\x00", 1], ["\x01\x00\x00\x00", "\x00\x00\x00\x00"], c),
  lambda c: test_part_function_0xd5d(dbg, ["\x41\x42\x43\x44", "\x02\x00\x00\x00", "\x08\x00\x00\x00", 1], ["\x01\x00\x00\x00", "\x00\x00\x00\x00"], c),
  lambda c: test_part_function_0xd5d(dbg, ["\x00\x00\x00\x00", "\x02\x00\x00\x00", "\x03\x00\x00\x00", 1], ["\xff\xff\xff\xff", "\x01\x00\x00\x00"], c),
  lambda c: test_part_function_0xd5d(dbg, ["\x00\x00\x00\x00", "\x42\x42\x42\x42", "\x42\x42\x42\x42", 1], ["\x00\x00\x00\x00", "\x01\x00\x00\x00"], c),
  lambda c: test_part_function_0xd5d(dbg, ["\x00\x00\x00\x00", "\x04\x00\x00\x00", "\x12\x00\x00\x00", 1], ["\x05\x00\x00\x00", "\xff\xff\xff\xff"], c),
  lambda c: test_part_function_0xd5d(dbg, ["\x00\x00\x00\x00", "\x12\x00\x00\x00", "\x04\x00\x00\x00", 1], ["\x01\x00\x00\x00", "\xfc\xff\xff\xff"], c),
  lambda c: test_part_function_0xd5d(dbg, ["\x00\x00\x00\x00", "\x13\x00\x00\x00", "\x06\x00\x00\x00", 1], ["\x01\x00\x00\x00", "\xfd\xff\xff\xff"], c),
  lambda c: test_part_function_0xd5d(dbg, ["\x00\x00\x00\x00", "\x00\x00\x00\x01", "\x00\x00\x01\x00", 1], ["\x00\x00\x00\x00", "\x01\x00\x00\x00"], c),
  lambda c: test_part_function_0xd5d(dbg, ["\x00\x00\x00\x00", "\x42\x00\x00\x00", "\x43\x00\x00\x00", 1], ["\xff\xff\xff\xff", "\x01\x00\x00\x00"], c),
  lambda c: test_part_function_0xd5d(dbg, ["\x00\x00\x00\x00\x00\x00\x00\x00", "\x00\x00\x00\x00\x42\x00\x00\x00", "\x00\x00\x00\x00\x43\x00\x00\x00", 2], ["\xff\xff\xff\xff\xff\xff\xff\xff", "\x00\x00\x00\x00\x01\x00\x00\x00"], c),
  lambda c: test_part_function_0xd5d(dbg, ["\x00\x00\x00\x00", "\x42\x42\x00\x00", "\x43\x43\x00\x00", 1], ["\x76\x31\x00\x00", "\x47\xcf\xff\xff"], c),
  lambda c: test_part_function_0xd5d(dbg, ["\x00\x00\x00\x00", "\x42\x42\x42\x00", "\x43\x43\x43\x00", 1], ["\x36\x76\x31\x00", "\xc7\x46\xcf\xff"], c),
  #lambda c: test_part_function_0xd5d(dbg, ["\x00\x00\x00\x00", "\x42\x42\x42\x42", "\x43\x43\x43\x43", 1], ["\x36\x36\x76\x31", "\xc7\xc6\x46\xcf"], c),
  lambda c: test_part_function_0xd5d(dbg, ["\x00\x00\x00\x00\x00\x00\x00\x00", "\x00\x00\x00\x00\x42\x42\x42\x42", "\x00\x00\x00\x00\x43\x43\x43\x43", 2], ["\x00\x00\x00\x00\x36\x36\x76\x31", "\xff\xff\xff\xff\xc7\xc6\x46\xcf"], c),
  lambda c: test_part_function_0xd5d(dbg, ["\x00\x00\x00\x00", "\x00\x13\x00\x00", "\x82\x0d\x00\x00", 1], ["\xb4\xfe\xff\xff", "\xd3\x01\x00\x00"], c),
  lambda c: test_part_function_0xd5d(dbg, ["\x00\x00\x00\x00", "\x78\x00\x00\x00", "\x17\x00\x00\x00", 1], ["\x0e\x00\x00\x00", "\xb7\xff\xff\xff"], c),
  #lambda c: test_part_function_0xd5d(dbg, ["\x00\x00\x00\x00\x00\x00\x00\x00", "\x42\x42\x42\x42\x42\x00\x00\x00", "\x43\x43\x43\x43\x43\x00\x00\x00", 2], ["\xc3\xc3\x03\xff\xff\xff\xbf\xc4", "\x78\x78\xf8\x00\x01\x00\x80\x77"], c),
  lambda c: test_part_function_0xd5d(dbg, ["\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", "\x00\x00\x00\x00\x42\x42\x42\x42\x42\x00\x00\x00", "\x00\x00\x00\x00\x43\x43\x43\x43\x43\x00\x00\x00", 3], ["\xff\xff\xff\xff\xc3\xc3\x03\xff\xff\xff\xbf\xc4", "\x00\x00\x00\x00\x78\x78\xf8\x00\x01\x00\x80\x77"], c),
  #lambda c: test_part_function_0xd5d(dbg, ["\x00\x00\x00\x00\x00\x00\x00\x00", "\x42\x42\x42\x42\x42\x42\x00\x00", "\x43\x43\x43\x43\x43\x43\x00\x00", 2], ["\x36\x36\x76\x31\x76\x31\xc0\x04", "\xc6\xc6\x46\xcf\x47\xcf\x7f\xf7"], c),
  lambda c: test_part_function_0xd5d(dbg, ["\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", "\x00\x00\x00\x00\x42\x42\x42\x42\x42\x42\x00\x00", "\x00\x00\x00\x00\x43\x43\x43\x43\x43\x43\x00\x00", 3], ["\x00\x00\x00\x00\x36\x36\x76\x31\x76\x31\xc0\x04", "\xff\xff\xff\xff\xc6\xc6\x46\xcf\x47\xcf\x7f\xf7"], c),
  #lambda c: test_part_function_0xd5d(dbg, ["\x00\x00\x00\x00\x00\x00\x00\x00", "\x42\x42\x42\x42\x42\x42\x42\x00", "\x43\x43\x43\x43\x43\x43\x43\x00", 2], ["\x36\x36\x76\x31\x36\x76\xf1\x04", "\xc6\xc6\x46\xcf\xc7\x46\x4f\xf7"], c),
  lambda c: test_part_function_0xd5d(dbg, ["\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", "\x00\x00\x00\x00\x42\x42\x42\x42\x42\x42\x42\x00", "\x00\x00\x00\x00\x43\x43\x43\x43\x43\x43\x43\x00", 3], ["\x00\x00\x00\x00\x36\x36\x76\x31\x36\x76\xf1\x04", "\xff\xff\xff\xff\xc6\xc6\x46\xcf\xc7\x46\x4f\xf7"], c),
  #lambda c: test_part_function_0xd5d(dbg, ["\x00\x00\x00\x00\x00\x00\x00\x00", "\x42\x42\x42\x42\x42\x42\x42\x42", "\x43\x43\x43\x43\x43\x43\x43\x43", 2], ["\x36\x36\x76\x31\x36\x36\x36\x36", "\xc6\xc6\x46\xcf\xc7\xc6\xc6\xc6"], c)
  lambda c: test_part_function_0xd5d(dbg, ["\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00", "\x00\x00\x00\x00\x42\x42\x42\x42\x42\x42\x42\x42", "\x00\x00\x00\x00\x43\x43\x43\x43\x43\x43\x43\x43", 3], ["\x00\x00\x00\x00\x36\x36\x76\x31\x36\x36\x36\x36", "\xff\xff\xff\xff\xc6\xc6\x46\xcf\xc7\xc6\xc6\xc6"], c),
  
  lambda c: test_full_function_0xd5d(dbg, ["\x00\x00\x00\x00\x00\x00\x00\x00", "\x00\x00\x00\x00\x42\x42\x42\x42", "\x00\x00\x00\x00\x43\x43\x43\x43", 2], "\x00\x00\x00\x00\x09\x09\x89\x11", c),
  lambda c: test_full_function_0xd5d(dbg, ["\x00\x00\x00\x00", "\x00\x13\x00\x00", "\x82\x0d\x00\x00", 1], "\xd3\x01\x00\x00", c),
  lambda c: test_full_function_0xd5d(dbg, ["\x00\x00\x00\x00", "\x81\x0d\x00\x00", "\x00\x13\x00\x00", 1], "\xe8\x03\x00\x00", c)
  ]
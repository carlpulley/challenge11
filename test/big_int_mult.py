def test_function_0xba3(dbg, args, expected, continuation):
  # big_int_mult testing
  bigint_eax, bigint_ecx, bigint_edx, bigint_arg0, bigint_arg1, num_arg2 = args
  ESP = dbg.context.Esp - 6*(num_arg2*4+4) - 4*3
  dbg.set_register("ESP", ESP)
  bigint_arg0_addr = ESP+(num_arg2*4+4)*1+4*3
  bigint_arg1_addr = ESP+(num_arg2*4+4)*2+4*3
  bigint_eax_addr = ESP+(num_arg2*4+4)*3+4*3
  bigint_ecx_addr = ESP+(num_arg2*4+4)*4+4*3
  bigint_edx_addr = ESP+(num_arg2*4+4)*5+4*3
  dbg.set_register("EAX", bigint_eax_addr)
  dbg.set_register("ECX", bigint_ecx_addr)
  dbg.set_register("EDX", bigint_edx_addr)
  dbg.write_process_memory(bigint_arg0_addr, bigint_arg0)
  dbg.write_process_memory(bigint_arg1_addr, bigint_arg1)
  dbg.write_process_memory(bigint_eax_addr, bigint_eax)
  dbg.write_process_memory(bigint_ecx_addr, bigint_ecx)
  dbg.write_process_memory(bigint_edx_addr, bigint_edx)
  dbg.write_process_memory(ESP+4, struct.pack("L", bigint_arg0_addr))
  dbg.write_process_memory(ESP+8, struct.pack("L", bigint_arg1_addr))
  dbg.write_process_memory(ESP+0xC, struct.pack("L", num_arg2))
  def check_test(d):
    actual = d.read_process_memory(bigint_eax_addr, num_arg2*4)
    if actual == expected:
      d.out.write("PASSED\n")
    else:
      d.out.write("*** FAILED! ***\n\n")
      d.out.write("Stage 2 testing: function 0xBA3 (big_int_mult) called with arguments:\n")
      d.out.write("\nregister arg0 (EAX: *big_int = 0x%08X):\n"%bigint_eax_addr)
      d.out.write(d.hex_dump(bigint_eax))
      d.out.write("\nregister arg1 (ECX: *big_int = 0x%08X):\n"%bigint_ecx_addr)
      d.out.write(d.hex_dump(bigint_ecx))
      d.out.write("\nregister arg2 (EDX: *big_int = 0x%08X):\n"%bigint_edx_addr)
      d.out.write(d.hex_dump(bigint_edx))
      d.out.write("\nfunction arg0 ([ESP]: *big_int = 0x%08X):\n"%bigint_arg0_addr)
      d.out.write(d.hex_dump(bigint_arg0))
      d.out.write("\nfunction arg1 ([ESP+4]: *big_int = 0x%08X):\n"%bigint_arg1_addr)
      d.out.write(d.hex_dump(bigint_arg1))
      d.out.write("\nfunction arg2 ([ESP+8]: *big_int = %d):\n"%num_arg2)
      d.out.write("\nEXPECTED:\n")
      d.out.write(d.hex_dump(expected))
      d.out.write("\nFOUND:\n")
      d.out.write(d.hex_dump(actual)+"\n")
      d.out.write(d.dump_context(d.get_thread_context(thread_handle = d.h_thread))+"\n")
    d.out.write("\n")
  function_test(dbg, 0xba3, 0xd3c, check_test, continuation)

tests = [  # big_int_mult tests
  lambda c: test_function_0xba3(dbg, ["\x00\x00\x00\x00\x01\x00\x00\x00", "\x00\x00\x00\x00\x10\x00\x00\x00", "\x00\x00\x00\x00\x41\x41\x41\x00", "\x00\x00\x00\x00\x00\x00\x00\x00", "\x00\x00\x00\x00\x00\x00\x00\x00", 2], "\x46\x46\x46\x46\x46\x46\x46\x46", c),
  lambda c: test_function_0xba3(dbg, ["\x00\x00\x00\x00\x00\x00\x00\x00", "\x42\x42\x42\x42\x42\x42\x42\x42", "\x43\x43\x43\x43\x43\x43\x43\x43", "\x44\x44\x44\x44\x44\x44\x44\x44", "\x00\x00\x00\x00\x00\x00\x00\x00", 2], "\x46\x46\x46\x46\x46\x46\x46\x46", c),
  lambda c: test_function_0xba3(dbg, ["\x00\x00\x00\x00\x00\x00\x00\x00", "\x00\x00\x00\x00\xa0\x16\x00\x00", "\x00\x00\x00\x00\xcd\x04\x00\x00", "\x00\x00\x00\x00\xbf\x1b\x01\x00", "\xff\xff\xff\xff\x76\x02\xff\xff", 2], "\x46\x46\x46\x46\x46\x46\x46\x46", c)
  ]
  
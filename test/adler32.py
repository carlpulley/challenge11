def simulate_hash_0xd4(msg):
  s = StringIO.StringIO(msg)
  return zlib.adler32(s.read())

def test_adler32_function_0xd4(dbg, args, expected, continuation):
    # adler32 testing
    [msg] = args
    msg_len = len(msg)
    ESP = dbg.context.Esp
    msg_addr = 0x284ad000
    dbg.write_process_memory(msg_addr, msg)
    dbg.write_process_memory(ESP+0x38, struct.pack("L", msg_addr))
    dbg.write_process_memory(ESP+0x2c, struct.pack("L", msg_len))
    def check_test(d):
      actual = d.read_process_memory(d.context.Esp+0x154, 4)
      if actual == expected:
        d.out.write("PASSED\n")
      else:
        d.out.write("*** FAILED! ***\n\n")
        d.out.write("Stage 2 testing: function 0xD4 (adler32) called with arguments:\n")
        d.out.write("\nmsg ([ESP+0x2C] = 0x%08X):\n"%msg_addr)
        d.out.write(d.hex_dump(msg))
        d.out.write("\nmsg length ([ESP+0x38] = %d):\n"%msg_len)
        d.out.write("\nEXPECTED:\n")
        d.out.write(d.hex_dump(expected))
        d.out.write("\nFOUND:\n")
        d.out.write(d.hex_dump(actual))
      d.out.write("\n")
    function_test(dbg, 0x430, 0x58c, check_test, continuation)

tests = [ # adler32 tests
    lambda c: test_adler32_function_0xd4(dbg, ["\x44\x43\x42\x41"], "\x0b\x01\xa2\x02", c),
    lambda c: test_adler32_function_0xd4(dbg, ["\x41\x42\x43\x44"], "\x0b\x01\x98\x02", c)
    ]
    
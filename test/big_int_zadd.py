def test_function_0xb44(dbg, args, expected, continuation):
  # big_int_zadd testing
  arg0, digit1, digit0, num_longs = args
  ESP = dbg.context.Esp - 2*(num_longs*4+4)
  dbg.set_register("ESP", ESP)
  arg0_addr = ESP+4
  eax_big_int_addr = ESP+num_longs*4+4
  dbg.write_process_memory(eax_big_int_addr, arg0)
  dbg.set_register("EAX", eax_big_int_addr)
  dbg.set_register("ECX", int(digit0.encode("hex"), 16))
  dbg.set_register("EDX", int(digit1.encode("hex"), 16))
  dbg.write_process_memory(arg0_addr, struct.pack("L", num_longs))
  def check_test(d):
    actual = d.read_process_memory(d.context.Eax, num_longs*4)
    if actual == expected:
      d.out.write("PASSED\n")
    else:
      d.out.write("*** FAILED! ***\n\n")
      d.out.write("Stage 2 testing: function 0xB44 (big_int_zadd) called with arguments:\n")
      d.out.write("\narg0 (*EAX):\n")
      d.out.write(d.hex_dump(dbg.read_process_memory(arg0_addr, num_longs*4)))
      d.out.write("\ndigit0 (ECX): 0x%08X\n"%int(digit0.encode("hex"), 16))
      d.out.write("\ndigit1 (EDX): 0x%08X\n"%int(digit1.encode("hex"), 16))
      d.out.write("\nnumber of longs (ESP+4): 0x%08X\n"%struct.unpack("L", d.read_process_memory(arg0_addr, 4)))
      d.out.write("\nEXPECTED:\n")
      d.out.write(d.hex_dump(expected))
      d.out.write("\nFOUND:\n")
      d.out.write(d.hex_dump(actual))
    d.out.write("\n")
  function_test(dbg, 0xb44, 0xb5e, check_test, continuation)

tests = [ # big_int_zadd tests
    lambda c: test_function_0xb44(dbg, ["\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04", "\x00\x00\x00\x00", "\x00\x00\x00\x00", 5], "\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04", c),
    lambda c: test_function_0xb44(dbg, ["\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04", "\x00\x00\x00\x00", "\x01\x00\x00\x00", 5], "\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x05", c),
    lambda c: test_function_0xb44(dbg, ["\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04", "\x00\x00\x00\x01", "\x00\x00\x00\x00", 5], "\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04\x02\x02\x03\x04\x01\x02\x03\x04", c),
    lambda c: test_function_0xb44(dbg, ["\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04\x80\x02\x03\x04\x01\x02\x03\x04", "\x00\x00\x00\x80", "\x00\x00\x00\x00", 5], "\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04\x00\x03\x03\x04\x01\x02\x03\x04", c),
    lambda c: test_function_0xb44(dbg, ["\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x80\x01\x02\x03\x04", "\x80\x00\x00\x00", "\x00\x00\x00\x00", 5], "\x01\x02\x03\x04\x01\x02\x03\x04\x02\x02\x03\x04\x01\x02\x03\x00\x01\x02\x03\x04", c)
    ]
    
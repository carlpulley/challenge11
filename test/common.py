def to_bytestream(hex_str, n):
  assert(len(hex_str) == 8*n)
  bytestrm = []
  for d in range(0, len(hex_str), 8):
    digit = []
    for i in range(0, 8, 2):
      digit = [hex_str[d:d+8][i:i+2]] + digit
    bytestrm.append(join(digit, ""))
  return join(bytestrm, "")
    
def bigint_to_int(bigint, n):
  return int(to_bytestream(bigint, n), 16)
    
def int_to_bigint(i, n):
  hexstr = hex(i)
  hexstr = "0"*((8 - (len(hexstr) % 8)) % 8) + hexstr
  padded_hexstr = "00"*4*(n - (len(hexstr) // 8)) + hexstr
  return to_bytestream(padded_hexstr, n)

# TODO: convert hex string to a byte string (for directly writing to memory!)

def process_tests(tests):
  if tests != []:
    tests[0](tests[1:])

def function_hook(dbg, exit_addr, check_stack, tests):
  if dbg.dbg.u.Exception.dwFirstChance:
    check_stack(dbg)
    dbg.bp_del(expected_stage3_data_addr + exit_addr)
    if tests != []:
      process_tests(tests)
    return DBG_CONTINUE
  else:
    dbg.out.write("Stage 2 testing: terminating process unexpectably!\n")
    dbg.terminate_process()
    return DBG_EXCEPTION_NOT_HANDLED

def function_test(dbg, entry_addr, exit_addr, check_stack, tests):
  dbg.set_register("EIP", expected_stage3_data_addr + entry_addr)
  dbg.out.write("Test #%d: function exit hook set at 0x%08X [+0x%X]\n" % (len(tests)+1, expected_stage3_data_addr + exit_addr, exit_addr))
  dbg.out.write("\n")
  dbg.bp_set(expected_stage3_data_addr + exit_addr, handler = lambda d: function_hook(d, exit_addr, check_stack, tests))

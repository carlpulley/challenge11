def test_function_0x1026(dbg, args, expected, continuation):
  # big_int_exp testing
  bigint_arg0, bigint_arg1, bigint_arg2, bigint_arg3, bigint_arg4, bigint_arg5, bigint_arg6, num_arg7 = args
  ESP = dbg.context.Esp - 6*(num_arg2*4+4) - 4*3
  dbg.set_register("ESP", ESP)
  bigint_arg0_addr = ESP+(num_arg2*4+4)*1+4*3
  bigint_arg1_addr = ESP+(num_arg2*4+4)*2+4*3
  bigint_arg2_addr = ESP+(num_arg2*4+4)*3+4*3
  bigint_arg3_addr = ESP+(num_arg2*4+4)*4+4*3
  bigint_arg4_addr = ESP+(num_arg2*4+4)*5+4*3
  bigint_arg5_addr = ESP+(num_arg2*4+4)*5+4*3
  bigint_arg6_addr = ESP+(num_arg2*4+4)*5+4*3
  dbg.write_process_memory(bigint_arg0_addr, bigint_arg0)
  dbg.write_process_memory(bigint_arg1_addr, bigint_arg1)
  dbg.write_process_memory(bigint_arg2_addr, bigint_arg2)
  dbg.write_process_memory(bigint_arg3_addr, bigint_arg3)
  dbg.write_process_memory(bigint_arg4_addr, bigint_arg4)
  dbg.write_process_memory(bigint_arg5_addr, bigint_arg5)
  dbg.write_process_memory(bigint_arg6_addr, bigint_arg6)
  dbg.write_process_memory(ESP+4, struct.pack("L", bigint_arg0_addr))
  dbg.write_process_memory(ESP+8, struct.pack("L", bigint_arg1_addr))
  dbg.write_process_memory(ESP+0xC, struct.pack("L", bigint_arg2_addr))
  dbg.write_process_memory(ESP+0x10, struct.pack("L", bigint_arg3_addr))
  dbg.write_process_memory(ESP+0x14, struct.pack("L", bigint_arg4_addr))
  dbg.write_process_memory(ESP+0x18, struct.pack("L", bigint_arg5_addr))
  dbg.write_process_memory(ESP+0x1C, struct.pack("L", bigint_arg6_addr))
  dbg.write_process_memory(ESP+0x20, struct.pack("L", num_arg7))
  def check_test(d):
    actual = d.read_process_memory(bigint_arg0_addr, num_arg7*4)
    if actual == expected:
      d.out.write("PASSED\n")
    else:
      d.out.write("*** FAILED! ***\n\n")
      d.out.write("Stage 2 testing: function 0x1026 (big_int_exp) called with arguments:\n")
      d.out.write("\nfunction arg0 ([ESP]: *big_int = 0x%08X):\n"%bigint_arg0_addr)
      d.out.write(d.hex_dump(bigint_arg0))
      d.out.write("\nfunction arg1 ([ESP+4]: *big_int = 0x%08X):\n"%bigint_arg1_addr)
      d.out.write(d.hex_dump(bigint_arg1))
      d.out.write("\nfunction arg2 ([ESP+8]: *big_int = 0x%08X):\n"%bigint_arg2_addr)
      d.out.write(d.hex_dump(bigint_arg2))
      d.out.write("\nfunction arg3 ([ESP+0xC]: *big_int = 0x%08X):\n"%bigint_arg3_addr)
      d.out.write(d.hex_dump(bigint_arg3))
      d.out.write("\nfunction arg4 ([ESP+0x10]: *big_int = 0x%08X):\n"%bigint_arg4_addr)
      d.out.write(d.hex_dump(bigint_arg4))
      d.out.write("\nfunction arg5 ([ESP+0x14]: *big_int = 0x%08X):\n"%bigint_arg5_addr)
      d.out.write(d.hex_dump(bigint_arg5))
      d.out.write("\nfunction arg6 ([ESP+0x18]: *big_int = 0x%08X):\n"%bigint_arg6_addr)
      d.out.write(d.hex_dump(bigint_arg6))
      d.out.write("\nfunction arg7 ([ESP+0x1C]: *big_int = %d):\n"%num_arg7)
      d.out.write("\nEXPECTED:\n")
      d.out.write(d.hex_dump(expected))
      d.out.write("\nFOUND:\n")
      d.out.write(d.hex_dump(actual)+"\n")
      d.out.write(d.dump_context(d.get_thread_context(thread_handle = d.h_thread))+"\n")
    d.out.write("\n")
  function_test(dbg, 0x1026, 0x119d, check_test, continuation)

tests = [  # big_int_exp tests
  lambda c: test_function_0x1026(dbg, ["\x00\x00\x00\x00\x00\x00\x00\x00", "\x00\x00\x00\x00\xa0\x16\x00\x00", "\x00\x00\x00\x00\xcd\x04\x00\x00", "\x00\x00\x00\x00\xbf\x1b\x01\x00", "\xff\xff\xff\xff\x76\x02\xff\xff", "\x00\x00\x00\x00\xa0\x16\x00\x00", "\x00\x00\x00\x00\xa0\x16\x00\x00", 2], "\x46\x46\x46\x46\x46\x46\x46\x46", c)
  ]
  
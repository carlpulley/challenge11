def test_function_0xb8a(dbg, args, expected, continuation):
  # big_int_asr testing
  arg0, num_longs = args
  ESP = dbg.context.Esp - 2*(num_longs*4+4)
  dbg.set_register("ESP", ESP)
  arg0_addr = ESP
  dbg.write_process_memory(arg0_addr, arg0)
  dbg.set_register("EAX", arg0_addr)
  dbg.set_register("EDX", num_longs)
  def check_test(d):
    actual = d.read_process_memory(arg0_addr, num_longs*4)
    if actual == expected:
      d.out.write("PASSED\n")
    else:
      d.out.write("*** FAILED! ***\n\n")
      d.out.write("Stage 2 testing: function 0xB8A (big_int_sr) called with arguments:\n")
      d.out.write("\narg0 (*EAX):\n")
      d.out.write(d.hex_dump(arg0))
      d.out.write("\nnumber of longs (EDX): %d\n"%num_longs)
      d.out.write("\nEXPECTED:\n")
      d.out.write(d.hex_dump(expected))
      d.out.write("\nFOUND:\n")
      d.out.write(d.hex_dump(actual))
    d.out.write("\n")
  function_test(dbg, 0xb8a, 0xba2, check_test, continuation)
  
tests = [  # big_int_asr tests
  lambda c: test_function_0xb8a(dbg, ["\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04", 5], "\x00\x81\x01\x02\x00\x81\x01\x82\x00\x81\x01\x82\x00\x81\x01\x82\x00\x81\x01\x82", c),
  lambda c: test_function_0xb8a(dbg, ["\x80\x90\xa0\xb0\x80\x90\xa0\xb0\x80\x90\xa0\xb0\x80\x90\xa0\xb0\x80\x90\xa0\xb0", 5], "\x40\x48\x50\xd8\x40\x48\x50\x58\x40\x48\x50\x58\x40\x48\x50\x58\x40\x48\x50\x58", c)
  ]
  
def test_function_0xb81(dbg, args, expected, continuation):
  # big_int_lsl testing
  arg0, num_longs = args
  ESP = dbg.context.Esp - 2*(num_longs*4+4)
  dbg.set_register("ESP", ESP)
  arg0_addr = ESP
  dbg.write_process_memory(arg0_addr, arg0)
  dbg.set_register("EAX", arg0_addr)
  dbg.set_register("EDX", num_longs)
  def check_test(d):
    actual = d.read_process_memory(arg0_addr, num_longs*4)
    if actual == expected:
      d.out.write("PASSED\n")
    else:
      d.out.write("*** FAILED! ***\n\n")
      d.out.write("Stage 2 testing: function 0xB81 (big_int_sl) called with arguments:\n")
      d.out.write("\narg0 (*EAX):\n")
      d.out.write(d.hex_dump(arg0))
      d.out.write("\nnumber of longs (EDX): %d\n"%num_longs)
      d.out.write("\nEXPECTED:\n")
      d.out.write(d.hex_dump(expected))
      d.out.write("\nFOUND:\n")
      d.out.write(d.hex_dump(actual))
    d.out.write("\n")
  function_test(dbg, 0xb81, 0xb89, check_test, continuation)

tests = [  # big_int_lsl tests
  lambda c: test_function_0xb81(dbg, ["\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04\x01\x02\x03\x04", 5], "\x02\x04\x06\x08\x02\x04\x06\x08\x02\x04\x06\x08\x02\x04\x06\x08\x02\x04\x06\x08", c),
  lambda c: test_function_0xb81(dbg, ["\x80\x90\xa0\xb0\x80\x90\xa0\xb0\x80\x90\xa0\xb0\x80\x90\xa0\xb0\x80\x90\xa0\xb0", 5], "\x01\x21\x41\x61\x01\x21\x41\x61\x01\x21\x41\x61\x01\x21\x41\x61\x00\x21\x41\x61", c)
  ]
  